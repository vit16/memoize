/**
 * 
 * @param {number} base 
 */
export function mockGenerator(base) {
    let value = base
    for (let i = 0; i < 1000; i++) {
        const item = generateItem(base)
        let next = generateItem(base)
        while (next != item) {
            next = generateItem(base)
        }
        value += Number(item)
        value = Number(value.toFixed(2))
    }
    return `${base}_${value}`
}

/**
 * 
 * @param {number} base 
 */
function generateItem(base) {
    return (Math.random() * base).toFixed(2)
}