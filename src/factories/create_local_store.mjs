import { MemoizedItem } from '../repositories/memoized_item.mjs'

/**
 * @template {(...args: any[]) => any} T
 * @returns {import('../models/store.mjs').Istore<T>}
 */
export function createLocalStore() {
    /**
     * @param  {Parameters<T>} args
     */
    let serializeParameters = (...args) => {
        const serialized = JSON.stringify(args)
        return serialized
    }
    /**
     * @type {Map<string | number, MemoizedItem<ReturnType<T>>>}
     */
    let memory = new Map()
    return {
        get: (args) => {
            const serialized = serializeParameters(...args)
            return memory.get(serialized)
        },
        set: (key, value) => {
            const serialized = serializeParameters(...key)
            const item = new MemoizedItem(value)
            memory.set(serialized, item)
            return item
        },
        setRaw: (key, value) => {
            memory.set(key, value)
            return value
        },
        getRaw(key) {
            return memory.get(key)
        },
        removeRaw(key) {
            return memory.delete(key)
        },
        remove: (args) => {
            const serialized = serializeParameters(...args)
            return memory.delete(serialized)
        },
        get firstKey() {
            const keys = Array.from(memory.keys())
            return keys[0]
        },
        get first() {
            const keys = Array.from(memory.keys())
            return memory.get(keys[0])
        },
        removeFirst() {
            const keys = Array.from(memory.keys())
            const key = keys[0]
            const item = this.getRaw?.(key)
            memory.delete(key)
            return item
        },
        get length() {
            return memory.size
        },
    }
}