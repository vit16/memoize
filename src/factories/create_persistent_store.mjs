import fs from 'fs'
import path from 'path'
import { MemoizedItem } from '../repositories/memoized_item.mjs'
import { createLocalStore } from './create_local_store.mjs'

/**
 * @template {(...args: any[]) => any} T
 * @param {string} storeLocation
 * @returns {import('../models/store.mjs').Istore<T>}
 */
export function createPersistentStore(storeLocation) {
    const localStore = createLocalStore()

    /**
     * @param  {Parameters<T>} args
     */
    let serializeParameters = (...args) => {
        const serialized = JSON.stringify(args)
        return serialized
    }

    // Ensure the directory exists
    if (!fs.existsSync(storeLocation)) {
        fs.mkdirSync(storeLocation, { recursive: true })
    }

    // Load existing data from files if they exist
    if (fs.existsSync(storeLocation)) {
        const files = fs.readdirSync(storeLocation)
        const setRaw = localStore.setRaw
        if (!setRaw) throw new Error('Set raw is required')
        for (const file of files) {
            const filePath = path.join(storeLocation, file)
            const rawData = fs.readFileSync(filePath, 'utf8')
            const jsonObj = JSON.parse(rawData)
            const filename = path.basename(filePath)
            const key = filename.substring(0, filename.length - 5)
            setRaw(key, MemoizedItem.fromJsonObject(jsonObj))
        }
    }

    const saveToFile = (serializedKey, value) => {
        const filePath = path.join(storeLocation, `${serializedKey}.json`)
        fs.writeFileSync(filePath, JSON.stringify(value))
    }

    const removeFile = (serializedKey) => {
        const filePath = path.join(storeLocation, `${serializedKey}.json`)
        if (fs.existsSync(filePath)) {
            fs.unlinkSync(filePath)
        }
    }

    return {
        get: (args) => {
            return localStore.get(args)
        },
        set: (key, value) => {
            const serializedKey = serializeParameters(...key)
            const item = localStore.set(key, value)
            saveToFile(serializedKey, item)
            return item
        },
        setRaw: (key, value) => {
            throw new Error('Not implemented')
        },
        remove(args) {
            const serializedKey = serializeParameters(...args)
            return this.removeRaw?.(serializedKey) ?? false
        },
        removeRaw(key) {
            const result = localStore.removeRaw?.(key)
            if (result) {
                removeFile(key)
            }
            return result ?? false
        },
        get firstKey() {
            return localStore.firstKey
        },
        get first() {
            const key = this.firstKey
            if (key) {
                return localStore.getRaw?.(key)
            }
        },
        getRaw(key) {
            return localStore.getRaw?.(key)
        },
        removeFirst() {
            const key = this.firstKey
            if (key) {
                this.removeRaw?.(key)
                return this.getRaw?.(key)
            }
        },
        get length() {
            return localStore.length
        }
    }
}
