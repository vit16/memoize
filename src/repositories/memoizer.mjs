import debug from 'debug'
import { createLocalStore } from '../factories/create_local_store.mjs'
import { MemoizedItem } from './memoized_item.mjs'

const logInvoke = debug('memoizer:invoke')
const logRemoveFirst = debug('memoizer:removeWhenFull')
const logExpired = debug('memoizer:expired')

/**
 * @template {(...args: any[]) => any} T
 */
export class Memoizer {

    /**
     * @type {import('../models/memoize_options.mjs').ImemoizeOptions<T>}
     */
    options

    /**
     * @type {import('../models/store.mjs').Istore<T>}
     */
    store

    /**
     * 
     * @param {T} func 
     * @param {import('../models/memoize_options.mjs').ImemoizeOptions<T>} [options]
     */
    constructor(func, options = {}) {
        this.func = func
        this.options = options
        this.store = options.store ?? createLocalStore()
    }

    /**
     * 
     * @param  {Parameters<T>} args 
     * @returns {MemoizedItem<ReturnType<T>>}
     */
    invoke(...args) {
        let func = () => {
            const value = this.store.get(args)
            if (value) return value

            // Size validation
            this.removeIfFull()

            const generatedValue = this.func(...args)
            const item = this.store.set(args, generatedValue)
            return item
        }
        const found = func()
        logInvoke(`Found value: ${found.value}`)

        // Max age validation
        if (!found.hasValidAge(this.options.maxAge)) {
            logExpired(`${found.age}/${this.options.maxAge}: ${found.value}`)
            this.remove(...args)
            return this.invoke(...args)
        }

        found.incrementCall()
        return found
    }

    removeIfFull() {
        const max = this.options.maxItems
        const len = () => this.store.length
        while (max && len() >= max) {
            const removed = this.store.removeFirst()
            logRemoveFirst(`${len()}/${max}: ${removed?.value}`)
        }
    }

    /**
     * @param {Parameters<T>} args
     */
    get(...args) {
        const found = this.invoke(...args)
        return found.value
    }

    /**
     * 
     * @param  {Parameters<T>} args 
     */
    remove(...args) {
        return this.store.remove(args)
    }

    get memoized() {
        /**
         * @param {Parameters<T>} args
         */
        return (...args) => this.get(...args)
    }
}