
/**
 * @template T
 */
export class MemoizedItem {
    /**
     * @type {T}
     */
    value

    /**
     * @type {Date}
     */
    createdAt

    /**
     * The number of times the item was retrieved
     * @type {number}
     */
    retrieveTimes

    /**
     * 
     * @param {T} value
     * @param {object} [options]
     * @param {Date} [options.createdAt]
     * @param {number} [options.retrieveTimes]
     */
    constructor(value, options = {}) {
        this.value = value
        const { createdAt, retrieveTimes } = options
        this.createdAt = createdAt ?? new Date()
        this.retrieveTimes = retrieveTimes ?? 0
    }

    static fromJsonObject(obj) {
        const { value, createdAt, retrieveTimes } = obj
        return new MemoizedItem(value, {
            createdAt: new Date(createdAt),
            retrieveTimes
        })
    }

    incrementCall() {
        this.retrieveTimes = this.retrieveTimes + 1
    }

    get age() {
        return Date.now() - this.createdAt.getTime()
    }

    /**
     * 
     * @param {number | undefined} maxAge 
     */
    hasValidAge(maxAge) {
        if (maxAge == null) return true
        const age = this.age
        return age < maxAge
    }

    renew() {
        this.createdAt = new Date()
    }
}