import { MemoizedItem } from '../repositories/memoized_item.mjs'

/**
 * @template {(...args: any[]) => any} T
 * @typedef {object} Istore
 * @property {number} length
 * @property {IstoreGet<T>} get
 * @property {(key: string | number) => MemoizedItem<ReturnType<T>> | undefined} [getRaw]
 * @property {IstoreSet<T>} set
 * @property {IstoreRemove<T>} remove
 * @property {(key: string | number) => boolean} [removeRaw]
 * @property {IremoveFirst<T>} removeFirst
 * @property {MemoizedItem<ReturnType<T>> | undefined} [first]
 * @property {string | number | undefined} [firstKey]
 * @property {IstoreSetRaw<T>} [setRaw] 
 */

/**
 * @template {(...args: any[]) => any} T
 * @callback IstoreGet
 * @param {Parameters<T>} params
 * @returns {MemoizedItem<ReturnType<T>> | undefined}
 */

/**
 * @template {(...args: any[]) => any} T
 * @callback IstoreSet
 * @param {Parameters<T>} key
 * @param {ReturnType<T>} value
 * @returns {MemoizedItem<ReturnType<T>>}
 */

/**
 * @template {(...args: any[]) => any} T
 * @callback IstoreSetRaw
 * @param {string | number} key
 * @param {MemoizedItem<ReturnType<T>>} value
 */

/**
 * @template {(...args: any[]) => any} T
 * @callback IstoreRemove
 * @param {Parameters<T>} params
 * @returns {boolean}
 */

/**
 * @template {(...args: any[]) => any} T
 * @callback IremoveFirst
 * @returns {MemoizedItem<ReturnType<T>> | undefined}
 */