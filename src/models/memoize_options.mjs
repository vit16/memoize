/**
 * @template {(...args: any[]) => any} T
 * @typedef {object} ImemoizeOptions
 * @property {number} [maxAge]
 * @property {number} [maxItems]
 * @property {import('./store.mjs').Istore<T>} [store]
 */