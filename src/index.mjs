import { config } from 'dotenv'
import { mockGenerator } from './mock_generator.mjs'
import { Memoizer } from './repositories/memoizer.mjs'

config()

const memoize = new Memoizer(mockGenerator, {
    maxItems: 3,
    maxAge: 100,
})

/**
 * 
 * @param {number} base 
 */
function get(base) {
    const value = memoize.invoke(base)
    return value
}

get(1)
get(1)
get(2)
get(2)
get(1)
get(3)
get(4)
get(1)