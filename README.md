# Memoizer

The Memoizer class is designed to cache the results of function calls to improve performance. When you instantiate a Memoizer, you need to pass the target function you want to memoize (mockGenerator in this example) and an options object. The options object in this example includes maxAge, which specifies the maximum time (in milliseconds) a cached item should be considered valid.

### Caching Mechanism
* Cache Miss: When you invoke the function through the Memoizer instance for the first time with specific arguments, the result is not yet cached. The function runs, its result is cached, and then returned.

* Cache Hit: If you call the function again with the same arguments within the maxAge time frame, Memoizer returns the cached result instead of executing the function again.

### Expiration

The maxAge option specifies how long a cached item should be considered valid. If you try to get a cached value after its maxAge has passed, it's considered a "cache miss," and the function is invoked again to update the cache.

### Manual Operations
* Manual Value Change: You can directly manipulate the cached item's value through the store.get method, which allows for exceptional use-cases where you might want to alter cached values manually.

* Manual Item Removal: Memoizer provides a remove method to manually remove an item from the cache.

* Renew Item: You can also renew an item to avoid it being considered stale and subsequently removed from the cache, essentially resetting its age timer.

```js
import { mockGenerator } from './mock_generator.mjs'
import { Memoizer } from './repositories/memoizer.mjs'
import { wait } from './usecases/wait.mjs'

const memoize = new Memoizer(mockGenerator, {
    maxAge: 2000,
})

/**
 * @param {number} base 
 */
function get(base) {
    const begin = Date.now()
    const value = memoize.invoke(base)
    const end = Date.now()
    const elapsed = end - begin
    console.log({
        base,
        value: value.value,
        elapsed,
    })
    return value
}

get(1) // Cache miss
get(1) // Cache hit
get(2) // Cache miss
const twoItem = get(2) // Cache hit
get(1) // Cache hit

await wait(2000) // Wait to expire items

get(1) // Cache miss: item expired
const oneItem = memoize.store.get([1])
if (oneItem) oneItem.value = 5 // Manual item value change
get(1) // Cache hit

memoize.remove(1) // Manual removal of item in cache
get(1) // Cache miss
get(1) // Ceche hit
twoItem.renew() // Renew item to avoid cache miss
get(2) // Cache hit
```

## Customizable Storage with Memoizer

One of the standout features of the Memoizer class is its flexibility to work with custom storage solutions, allowing users to define how the caching mechanism stores and retrieves data. This is particularly useful if you need specialized storage behavior, like persisting data to disk, database, or even a distributed system.

### Using a Custom Store
To use a custom store, you can pass it in as the store property in the options object when you instantiate a Memoizer. Here's a brief example to demonstrate this:

```javascript
const customStore = /* Your Istore implementation here */
const memoize = new Memoizer(mockGenerator, {
    maxAge: 2000,
    store: customStore
});
```

### Istore Interface

The custom store should adhere to the Istore interface for seamless integration:

```typescript
interface Istore<T extends (...args: any[]) => any> {
    get: IstoreGet<T>;
    set: IstoreSet<T>;
    remove: IstoreRemove<T>;
}

type IstoreGet<T extends (...args: any[]) => any> = (
    params: Parameters<T>
) => MemoizedItem<ReturnType<T>> | undefined;

type IstoreSet<T extends (...args: any[]) => any> = (
    key: Parameters<T>,
    value: ReturnType<T>
) => MemoizedItem<ReturnType<T>>;

type IstoreRemove<T extends (...args: any[]) => any> = (
    params: Parameters<T>
) => boolean;
```

### Flexibility
Using a custom store allows you to:

- Implement complex storage and retrieval logic
- Easily switch between different storage engines (In-memory, disk, database, etc.)
- Control cache eviction policies beyond just time-based expiration
- Incorporate additional features like cache warming, analytics, and more.

By adhering to the Istore interface, your custom store can be used seamlessly with the Memoizer, offering a highly customizable caching solution.

### Example store: Using Map

```js
import { MemoizedItem } from '../repositories/memoized_item.mjs'

/**
 * @template {(...args: any[]) => any} T
 * @returns {import('../models/store.mjs').Istore<T>}
 */
export function createLocalStore() {
    /**
     * @param  {Parameters<T>} args
     */
    let serializeParameters = (...args) => {
        const serialized = JSON.stringify(args)
        return serialized
    }
    /**
     * @type {Map<string | number, MemoizedItem<ReturnType<T>>>}
     */
    let memory = new Map()
    return {
        get: (args) => {
            const serialized = serializeParameters(...args)
            return memory.get(serialized)
        },
        set: (key, value) => {
            const serialized = serializeParameters(...key)
            const item = new MemoizedItem(value)
            memory.set(serialized, item)
            return item
        },
        remove: (args) => {
            const serialized = serializeParameters(...args)
            return memory.delete(serialized)
        },
    }
}
```

## Road Map

[X] - Memoize function

[X] - Expire memoized item based on the given expiration

[X] - Allow max number of items to be stored

